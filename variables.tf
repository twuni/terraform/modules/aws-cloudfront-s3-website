variable "certificate_arn" {
  description = "The ARN of a certificate valid for this website domain."
  type        = string
}

variable "dns_zone" {
  description = "The ID of the Route 53 hosted zone to which the given domain belongs."
  type        = string
}

variable "domain" {
  description = "The hostname of the static website to be provisioned. Must match or be a subdomain of the given Route 53 hosted zone."
  type        = string
}

variable "tags" {
  default     = {}
  description = "Tags to be set on all resources provisioned by this module."
  type        = map(string)
}
