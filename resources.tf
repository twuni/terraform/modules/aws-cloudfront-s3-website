resource "aws_s3_bucket" "source" {
  bucket_prefix = "${var.domain}-"
  acl           = "private"

  website {
    index_document = "index.html"
    error_document = "index.html"
  }

  versioning {
    enabled = true
  }

  tags = merge(var.tags, {
    Name = var.domain
  })
}

resource "aws_cloudfront_distribution" "cdn" {
  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Website: ${var.domain}"
  default_root_object = "index.html"

  aliases = [
    var.domain,
    "www.${var.domain}",
  ]

  price_class  = "PriceClass_100"
  http_version = "http2"

  viewer_certificate {
    acm_certificate_arn      = var.certificate_arn
    minimum_protocol_version = "TLSv1.2_2018"
    ssl_support_method       = "sni-only"
  }

  default_cache_behavior {
    allowed_methods        = ["GET", "HEAD", "OPTIONS"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = "s3"
    viewer_protocol_policy = "redirect-to-https"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  origin {
    origin_id   = "s3"
    domain_name = aws_s3_bucket.source.website_endpoint

    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1.2"]
    }
  }

  tags = merge(var.tags, {
    Name = var.domain
  })
}

resource "aws_iam_policy" "writer" {
  name_prefix = "${var.domain}-writer"
  path        = "/"
  policy      = data.aws_iam_policy_document.writer.json

  tags = merge(var.tags, {
    Name = "${var.domain}-writer"
  })
}
