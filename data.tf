data "aws_iam_policy_document" "reader" {
  statement {
    actions = [
      "s3:ListBucket",
      "s3:ListBucketVersions"
    ]
    effect    = "Allow"
    resources = [aws_s3_bucket.source.arn]
    sid       = "ReadBucket"
  }

  statement {
    actions = [
      "s3:GetObject",
      "s3:GetObjectVersion",
    ]
    effect    = "Allow"
    resources = ["${aws_s3_bucket.source.arn}/*"]
    sid       = "ReadObjects"
  }
}

data "aws_iam_policy_document" "writer" {
  source_policy_documents = [data.aws_iam_policy_document.reader.json]

  statement {
    actions = [
      "s3:DeleteObject",
      "s3:DeleteObjectVersion",
      "s3:PutObject",
      "s3:PutObjectAcl",
      "s3:PutObjectVersionAcl"
    ]
    effect    = "Allow"
    resources = ["${aws_s3_bucket.source.arn}/*"]
    sid       = "WriteObjects"
  }
}
