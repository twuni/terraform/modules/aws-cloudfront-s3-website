output "dns_record" {
  description = "The domain name of this website's CloudFront distribution (CDN). Can be used to create a Route 53 alias record."
  value       = aws_cloudfront_distribution.cdn.domain_name
}

output "dns_zone" {
  description = "The ID of the Route 53 Hosted Zone for this website's CloudFront distribution (CDN). Can be used to create a Route 53 alias record."
  value       = aws_cloudfront_distribution.cdn.hosted_zone_id
}

output "writer_policy_arn" {
  description = "The ARN of an IAM policy granting read/write access to the S3 bucket from which this website is served."
  value       = aws_iam_policy.writer.arn
}
