# CloudFront S3 Website | AWS | Terraform Modules | Twuni

This Terraform module provisions a static website whose content
is hosted in an S3 bucket and served via a CloudFront distribution.
An IAM policy is also provisioned which grants read/write access
to the S3 bucket.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.0.3 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.52.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_certificate_arn"></a> [certificate\_arn](#input\_certificate\_arn) | The ARN of a certificate valid for this website domain. | `string` | n/a | yes |
| <a name="input_dns_zone"></a> [dns\_zone](#input\_dns\_zone) | The ID of the Route 53 hosted zone to which the given domain belongs. | `string` | n/a | yes |
| <a name="input_domain"></a> [domain](#input\_domain) | The hostname of the static website to be provisioned. Must match or be a subdomain of the given Route 53 hosted zone. | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to be set on all resources provisioned by this module. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_dns_record"></a> [dns\_record](#output\_dns\_record) | The domain name of this website's CloudFront distribution (CDN). Can be used to create a Route 53 alias record. |
| <a name="output_dns_zone"></a> [dns\_zone](#output\_dns\_zone) | The ID of the Route 53 Hosted Zone for this website's CloudFront distribution (CDN). Can be used to create a Route 53 alias record. |
| <a name="output_writer_policy_arn"></a> [writer\_policy\_arn](#output\_writer\_policy\_arn) | The ARN of an IAM policy granting read/write access to the S3 bucket from which this website is served. |
